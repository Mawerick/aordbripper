//
//  Copyright (C) Mawerick, WrongPlace.Net 2018
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using Twp.Utilities;
using Newtonsoft.Json;

namespace AORdbRipper
{
    [Serializable]
    public class Settings
    {
        static readonly string mPath = Path.Combine(PathExt.GetAppDataPath(), "AORdbRipper.settings");

        public string GamePath { get; set; }
        public string OutputPath { get; set; }

        public static Settings Load()
        {
            if (File.Exists(mPath))
            {
                var json = File.ReadAllText(mPath);
                return JsonConvert.DeserializeObject<Settings>(json);
            }
            return new Settings();
        }

        public void Save()
        {
            var json = JsonConvert.SerializeObject(this, Formatting.Indented);
            File.WriteAllText(mPath, json, System.Text.Encoding.UTF8);
        }
    }
}

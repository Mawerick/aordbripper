﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2018
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using System.Windows.Forms;
using Twp.Data.AO;
using Twp.FC.AO;
using Twp.Utilities;

namespace AORdbRipper
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            mGameRipTypesBox.Value = DataType.All;
            DatabaseManager.PreparseItems = true;
            DatabaseManager.FlattenReqs = true;
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            mGamePathBox.Text = Program.Settings.GamePath;
            mOutputPathBox.Text = Program.Settings.OutputPath;
            DatabaseManager.DataPath = Program.Settings.OutputPath;
            UpdateGame();
            UpdateRipButton();
        }

        void OnGamePathClicked(object sender, EventArgs e)
        {
            string path = mGamePathBox.Text;
            if (GamePath.Browse(ref path))
            {
                mGamePathBox.Text = path;
                Program.Settings.GamePath = path;
            }
            UpdateGame();
            UpdateRipButton();
        }

        void OnOutputPathClicked(object sender, EventArgs e)
        {
            string path = mOutputPathBox.Text;
            using (var dialog = new FolderBrowserDialog())
            {
                dialog.Description = "Select output path";
                if (!String.IsNullOrEmpty(path))
                    dialog.SelectedPath = path;
                if (dialog.ShowDialog() == DialogResult.OK)
                    path = dialog.SelectedPath;
            }
            mOutputPathBox.Text = path;
            Program.Settings.OutputPath = path;
            DatabaseManager.DataPath = Program.Settings.OutputPath;
            UpdateRipButton();
        }

        void OnRipClicked(object sender, EventArgs e)
        {
            string fileName = String.Format("{0}_{1}.db3", Application.ProductName, mGameVersionBox.Text.Replace('.', '_'));
            fileName = Path.Combine(Program.Settings.OutputPath, fileName);
            Log.Debug("[OnGameRipClicked] FileName: {0}", fileName);
            Database db = new Database(fileName);
            db.Extract(mGamePathBox.Text, 1000, (DataType)mGameRipTypesBox.Value);
            db.Close();
        }

        private void UpdateGame()
        {
            if (GamePath.IsValid(mGamePathBox.Text))
                mGameVersionBox.Text = DatabaseManager.GetVersion(mGamePathBox.Text);
            else
                mGameVersionBox.Text = null;
        }

        public void UpdateRipButton()
        {
            if (GamePath.IsValid(mGamePathBox.Text) && Directory.Exists(Program.Settings.OutputPath))
                mGameRipButton.Enabled = true;
            else
                mGameRipButton.Enabled = false;
        }
    }
}

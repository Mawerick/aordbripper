﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2018
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AORdbRipper
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mGameLine = new Twp.Controls.Line();
            this.mGameRipButton = new System.Windows.Forms.Button();
            this.mGameVersionBox = new System.Windows.Forms.TextBox();
            this.mGameVersionLabel = new System.Windows.Forms.Label();
            this.mGamePathBox = new System.Windows.Forms.TextBox();
            this.mGamePathLabel = new System.Windows.Forms.Label();
            this.mGamePathButton = new System.Windows.Forms.Button();
            this.mGameRipTypesBox = new Twp.Controls.FlagsListBox();
            this.mOutputPathBox = new System.Windows.Forms.TextBox();
            this.mOutputPathLabel = new System.Windows.Forms.Label();
            this.mOutputPathBrowse = new System.Windows.Forms.Button();
            this.line1 = new Twp.Controls.Line();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // mGameLine
            // 
            this.mGameLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mGameLine.Location = new System.Drawing.Point(0, 87);
            this.mGameLine.Name = "mGameLine";
            this.mGameLine.Size = new System.Drawing.Size(384, 10);
            // 
            // mGameRipButton
            // 
            this.mGameRipButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mGameRipButton.Enabled = false;
            this.mGameRipButton.Location = new System.Drawing.Point(237, 99);
            this.mGameRipButton.Name = "mGameRipButton";
            this.mGameRipButton.Size = new System.Drawing.Size(141, 49);
            this.mGameRipButton.TabIndex = 9;
            this.mGameRipButton.Text = "Rip";
            this.mGameRipButton.UseVisualStyleBackColor = true;
            this.mGameRipButton.Click += new System.EventHandler(this.OnRipClicked);
            // 
            // mGameVersionBox
            // 
            this.mGameVersionBox.BackColor = System.Drawing.SystemColors.Window;
            this.mGameVersionBox.Enabled = false;
            this.mGameVersionBox.Location = new System.Drawing.Point(79, 32);
            this.mGameVersionBox.Name = "mGameVersionBox";
            this.mGameVersionBox.ReadOnly = true;
            this.mGameVersionBox.Size = new System.Drawing.Size(91, 20);
            this.mGameVersionBox.TabIndex = 4;
            // 
            // mGameVersionLabel
            // 
            this.mGameVersionLabel.AutoSize = true;
            this.mGameVersionLabel.BackColor = System.Drawing.Color.Transparent;
            this.mGameVersionLabel.Location = new System.Drawing.Point(6, 35);
            this.mGameVersionLabel.Name = "mGameVersionLabel";
            this.mGameVersionLabel.Size = new System.Drawing.Size(45, 13);
            this.mGameVersionLabel.TabIndex = 3;
            this.mGameVersionLabel.Text = "Version:";
            // 
            // mGamePathBox
            // 
            this.mGamePathBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mGamePathBox.BackColor = System.Drawing.SystemColors.Window;
            this.mGamePathBox.Enabled = false;
            this.mGamePathBox.Location = new System.Drawing.Point(79, 6);
            this.mGamePathBox.Name = "mGamePathBox";
            this.mGamePathBox.ReadOnly = true;
            this.mGamePathBox.Size = new System.Drawing.Size(268, 20);
            this.mGamePathBox.TabIndex = 1;
            // 
            // mGamePathLabel
            // 
            this.mGamePathLabel.AutoSize = true;
            this.mGamePathLabel.BackColor = System.Drawing.Color.Transparent;
            this.mGamePathLabel.Location = new System.Drawing.Point(6, 9);
            this.mGamePathLabel.Name = "mGamePathLabel";
            this.mGamePathLabel.Size = new System.Drawing.Size(63, 13);
            this.mGamePathLabel.TabIndex = 0;
            this.mGamePathLabel.Text = "Game Path:";
            // 
            // mGamePathButton
            // 
            this.mGamePathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mGamePathButton.Location = new System.Drawing.Point(353, 4);
            this.mGamePathButton.Name = "mGamePathButton";
            this.mGamePathButton.Size = new System.Drawing.Size(25, 23);
            this.mGamePathButton.TabIndex = 2;
            this.mGamePathButton.Text = "...";
            this.mGamePathButton.UseVisualStyleBackColor = true;
            this.mGamePathButton.Click += new System.EventHandler(this.OnGamePathClicked);
            // 
            // mGameRipTypesBox
            // 
            this.mGameRipTypesBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mGameRipTypesBox.CheckOnClick = true;
            this.mGameRipTypesBox.FormattingEnabled = true;
            this.mGameRipTypesBox.Location = new System.Drawing.Point(6, 99);
            this.mGameRipTypesBox.Name = "mGameRipTypesBox";
            this.mGameRipTypesBox.ShowAll = false;
            this.mGameRipTypesBox.ShowNull = false;
            this.mGameRipTypesBox.Size = new System.Drawing.Size(225, 109);
            this.mGameRipTypesBox.TabIndex = 8;
            // 
            // mOutputPathBox
            // 
            this.mOutputPathBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mOutputPathBox.BackColor = System.Drawing.SystemColors.Window;
            this.mOutputPathBox.Enabled = false;
            this.mOutputPathBox.Location = new System.Drawing.Point(79, 65);
            this.mOutputPathBox.Name = "mOutputPathBox";
            this.mOutputPathBox.ReadOnly = true;
            this.mOutputPathBox.Size = new System.Drawing.Size(268, 20);
            this.mOutputPathBox.TabIndex = 6;
            // 
            // mOutputPathLabel
            // 
            this.mOutputPathLabel.AutoSize = true;
            this.mOutputPathLabel.BackColor = System.Drawing.Color.Transparent;
            this.mOutputPathLabel.Location = new System.Drawing.Point(6, 68);
            this.mOutputPathLabel.Name = "mOutputPathLabel";
            this.mOutputPathLabel.Size = new System.Drawing.Size(67, 13);
            this.mOutputPathLabel.TabIndex = 5;
            this.mOutputPathLabel.Text = "Output Path:";
            // 
            // mOutputPathBrowse
            // 
            this.mOutputPathBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mOutputPathBrowse.Location = new System.Drawing.Point(353, 63);
            this.mOutputPathBrowse.Name = "mOutputPathBrowse";
            this.mOutputPathBrowse.Size = new System.Drawing.Size(25, 23);
            this.mOutputPathBrowse.TabIndex = 7;
            this.mOutputPathBrowse.Text = "...";
            this.mOutputPathBrowse.UseVisualStyleBackColor = true;
            this.mOutputPathBrowse.Click += new System.EventHandler(this.OnOutputPathClicked);
            // 
            // line1
            // 
            this.line1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.line1.Location = new System.Drawing.Point(0, 54);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(384, 10);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(237, 193);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Copyright © Mawerick 2018";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AORdbRipper.Properties.Resources.gplv3_88x31;
            this.pictureBox1.Location = new System.Drawing.Point(290, 154);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(88, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 214);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.line1);
            this.Controls.Add(this.mOutputPathBox);
            this.Controls.Add(this.mOutputPathLabel);
            this.Controls.Add(this.mOutputPathBrowse);
            this.Controls.Add(this.mGameRipTypesBox);
            this.Controls.Add(this.mGameLine);
            this.Controls.Add(this.mGameRipButton);
            this.Controls.Add(this.mGameVersionBox);
            this.Controls.Add(this.mGameVersionLabel);
            this.Controls.Add(this.mGamePathBox);
            this.Controls.Add(this.mGamePathLabel);
            this.Controls.Add(this.mGamePathButton);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "AO RDB Ripper";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Twp.Controls.Line mGameLine;
        private System.Windows.Forms.Button mGameRipButton;
        private System.Windows.Forms.TextBox mGameVersionBox;
        private System.Windows.Forms.Label mGameVersionLabel;
        private System.Windows.Forms.TextBox mGamePathBox;
        private System.Windows.Forms.Label mGamePathLabel;
        private System.Windows.Forms.Button mGamePathButton;
        private Twp.Controls.FlagsListBox mGameRipTypesBox;
        private System.Windows.Forms.TextBox mOutputPathBox;
        private System.Windows.Forms.Label mOutputPathLabel;
        private System.Windows.Forms.Button mOutputPathBrowse;
        private Twp.Controls.Line line1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}